import React from "react";

const CartPrice = ({ item }) => {
  return (
    <div className="flex content-end items-center justify-between">
      <p className="text-base font-semibold text-center text-gray-900">
        ID. {item.id}
      </p>
      <p>
        {(5.99).toLocaleString("en-US", {
          style: "currency",
          currency: "USD",
        })}
      </p>
    </div>
  );
};

export default CartPrice;
