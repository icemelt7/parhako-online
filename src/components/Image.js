import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Context } from "../Context";

const Image = ({ picture }) => {
  const { toggleFavorite, addToCart, removeFromCart } = useContext(Context);

  return (
    <div className="relative p-8 pt-0 z-10 rouned-lg">
      <img
        src={picture.url}
        alt={picture.url}
        className="h-64 w-64 rounded-lg"
      />
      <div className="absolute left-0 top-0 p-8 pt-0">
        <div className="h-64 w-64 bg-white opacity-0 hover:opacity-50 transition duration-500 ease-in-out z-0">
          <div className="absolute left-0 top-0 p-8 pt-2 ml-6 h-6 w-6">
            <div className="flex w-16 items-center justify-between">
              {picture.isFavorite ? (
                <svg
                  className="h-6 w-6 fill-current text-red-500 inline-block"
                  onClick={() => toggleFavorite(picture.id)}
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path
                    fillRule="evenodd"
                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                    clipRule="evenodd"
                  />
                </svg>
              ) : (
                <svg
                  className="w-6 h-6 stroke-current stroke-2 hover:text-red-700"
                  onClick={() => toggleFavorite(picture.id)}
                  fill="none"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path>
                </svg>
              )}
              {picture.inCart ? (
                <svg
                  className="w-6 h-6 fill-current text-green-700 stroke-current stroke-2"
                  onClick={() => removeFromCart(picture)}
                  fill="none"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                </svg>
              ) : (
                <svg
                  className="w-6 h-6 stroke-current stroke-2 hover:text-yellow-700"
                  onClick={() => addToCart(picture)}
                  fill="none"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                </svg>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Image.propTypes = {
  className: PropTypes.string,
  img: PropTypes.shape({
    id: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    isFavorite: PropTypes.bool,
  }),
};
export default Image;
