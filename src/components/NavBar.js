import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Context } from "../Context";

const NavLink = () => {
  const { cartItems } = useContext(Context);
  return (
    <ul className="flex items-center">
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/">Pic Some</Link>
      </li>
      {/* <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/photos">Photos</Link>
      </li> */}
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/cart">
          {cartItems.length < 1 ? (
            <div className="flex items-center">
              <svg
                className="w-6 h-6 stroke-current stroke-2 "
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
              </svg>
              <p className="text-sm font-semibold font-sans ml-2 text-white bg-red-600 rounded-lg w-6 h-6 items-center">
                {cartItems.length}
              </p>
            </div>
          ) : (
            <div className="flex items-center">
              <svg
                className="w-6 h-6 stroke-current stroke-2 fill-current"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
              </svg>

              <p className="text-sm font-semibold font-sans ml-2 text-white bg-red-600 rounded-lg w-6 h-6 items-center">
                {cartItems.length}
              </p>
            </div>
          )}
        </Link>
      </li>
    </ul>
  );
};

const NavBar = () => {
  const [open, setOpen] = React.useState(false);
  let show = "";

  function handleClick() {
    setOpen(!open);
  }

  show = open ? "block" : "hidden";

  return (
    <nav className="flex items-center justify-between flex-wrap bg-blue-500 ">
      <div className="block sm:hidden">
        <button
          className="fill-current flex items-center px-3 py-2 border rounded text-white border-white hover:text-white hover:border-white mr-4"
          onClick={handleClick}
        >
          <svg
            className="h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      <div
        className={`w-full ${show} flex-grow sm:flex sm:items-center sm:w-auto`}
      >
        <div className="text-sm sm:flex-grow sm:flex sm:justify-center">
          <NavLink />
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
