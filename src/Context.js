import React, { useState, useEffect } from "react";
const Context = React.createContext();

function ContextProvider({ children }) {
  const [photos, setPhotos] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [order, setOrder] = useState(true);
  const [placed, setPlaced] = useState(false);

  const url =
    "https://raw.githubusercontent.com/bobziroll/scrimba-react-bootcamp-images/master/images.json";
  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setPhotos(data));
  }, []);

  const toggleFavorite = (id) => {
    setPhotos(
      photos.map((photo) => {
        if (photo.id === id) {
          photo.isFavorite = !photo.isFavorite;
        }
        return photo;
      })
    );
    console.log("Marked as fav");
  };

  const addToCart = (picture) => {
    picture.inCart = true; //set photo inCart to true
    const finalState = [{ ...picture }, ...cartItems]; //push the new item to cart array
    setCartItems(finalState); //set the cart array
  };

  const removeFromCart = (picture) => {
    photos.map((photo) => {
      if (photo.id === picture.id) photo.inCart = false; //find matching photo and compare it with cart photo id. Set inCart to false in photo array
    });
    const newArray = cartItems.filter((item) => {
      if (item.id !== picture.id) return item; //remove the item from cart
    });
    setCartItems(newArray);
  };

  const removeAllFromCart = () => {
    photos.map((photo) => {
      photo.inCart = false;
    });
    setCartItems([]);
  };

  const placeOrder = () => {
    setOrder(false);
    setTimeout(() => {
      setOrder(true);
      setPlaced(true);
      setTimeout(() => {
        photos.map((photo) => {
          photo.inCart = false;
        });
        setCartItems([]);
        setPlaced(false);
        console.log("Order Placed");
      }, 1000);
    }, 3000);
  };

  return (
    <Context.Provider
      value={{
        photos,
        toggleFavorite,
        addToCart,
        removeFromCart,
        removeAllFromCart,
        placeOrder,
        placed,
        order,
        cartItems,
      }}
    >
      {children}
    </Context.Provider>
  );
}

export { ContextProvider, Context };
